<?php

namespace IPC\SecurityBundle\DependencyInjection;

use IPC\SecurityBundle\Form\Type\ChangePasswordType;
use IPC\SecurityBundle\Form\Type\LoginType;
use IPC\SecurityBundle\IPCSecurityBundle;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * {@inheritdoc}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root(IPCSecurityBundle::BUNDLE_PREFIX);
        $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('login')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('route')
                            ->defaultValue('login')
                        ->end()
                        ->scalarNode('form')
                            ->defaultValue(LoginType::class)
                        ->end()
                        ->scalarNode('view')
                            ->defaultValue('@IPCSecurity/login.html.twig')
                        ->end()
                        ->arrayNode('flash_bag')
                            ->addDefaultsIfNotSet()
                            ->canBeEnabled()
                            ->children()
                                ->booleanNode('translate')
                                    ->defaultTrue()
                                ->end()
                                ->arrayNode('type')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('error')
                                            ->defaultValue('error')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('credentials_expired')
                    ->addDefaultsIfNotSet()
                    ->canBeEnabled()
                    ->children()
                        ->scalarNode('route')
                            ->defaultValue('credentials_expired')
                        ->end()
                        ->scalarNode('target_route')
                            ->defaultValue('login')
                        ->end()
                        ->scalarNode('form')
                            ->defaultValue(ChangePasswordType::class)
                        ->end()
                        ->arrayNode('form_options')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->booleanNode('require_current')
                                    ->defaultFalse()
                                ->end()
                                ->booleanNode('require_repeated')
                                    ->defaultTrue()
                                ->end()
                            ->end()
                        ->end()
                        ->scalarNode('view')
                            ->defaultValue('@IPCSecurity/change_password.html.twig')
                        ->end()
                        ->arrayNode('flash_bag')
                            ->addDefaultsIfNotSet()
                            ->canBeEnabled()
                            ->children()
                                ->booleanNode('translate')
                                    ->defaultTrue()
                                ->end()
                                ->arrayNode('type')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('error')
                                            ->defaultValue('error')
                                        ->end()
                                        ->scalarNode('success')
                                            ->defaultValue('success')
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
        return $treeBuilder;
    }
}
