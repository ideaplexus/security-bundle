<?php

namespace IPC\SecurityBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod(Request::METHOD_POST)
            ->add(
                'username',
                TextType::class,
                [
                    'label'    => 'form.type.login.username.label',
                    'required' => false,
                ]
            )
            ->add(
                'password',
                PasswordType::class,
                [
                    'label'    => 'form.type.login.password.label',
                    'required' => false,
                ]
            )
            ->add(
                'submit',
                SubmitType::class,
                [
                    'label' => 'form.type.login.submit.label',
                ]
            );

        if (!empty($options['remember_me'])) {
            $builder
                ->add(
                'remember_me',
                CheckboxType::class,
                [
                    'label'    => 'form.type.login.remember_me.label',
                    'required' => false,
                ]
            );
        }
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     *
     * @throws AccessException
     * @throws UndefinedOptionsException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('remember_me', true)
            ->setAllowedTypes('remember_me', 'bool');
    }
}
