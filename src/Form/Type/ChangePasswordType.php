<?php

namespace IPC\SecurityBundle\Form\Type;

use IPC\SecurityBundle\Form\Model\ChangePassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraint;

class ChangePasswordType extends AbstractType
{
    /**
     * @var array
     */
    protected $validationGroups = [Constraint::DEFAULT_GROUP];

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['require_current']) {
            $this->validationGroups[] = 'require_current';
            $builder
                ->add(
                    'current',
                    PasswordType::class,
                    [
                        'label'    => 'form.type.change_password.current.label',
                        'required' => false,
                    ]
                );
        }

        if ($options['require_repeated']) {
            $this->validationGroups[] = 'require_repeated';
            $builder
                ->add(
                    'new',
                    RepeatedType::class,
                    [
                        'type'            => PasswordType::class,
                        'required'        => false,
                        'first_name'      => 'new',
                        'second_name'     => 'repeated',
                        'first_options'   => [
                            'label' => 'form.type.change_password.new.label',
                        ],
                        'second_options'  => [
                            'label' => 'form.type.change_password.repeated.label',
                        ],
                        'invalid_message' => 'form.validation.change_password.new.invalid_message',
                    ]
                );
        } else {
            $builder
                ->add(
                    'new',
                    PasswordType::class,
                    [
                        'label'    => 'form.type.change_password.new.label',
                        'required' => false,
                    ]
                );
        }

        $builder
            ->add(
                'update',
                SubmitType::class,
                [
                    'label' => 'form.type.change_password.update.label'
                ]
            )
            ->setMethod(Request::METHOD_POST);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     *
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefault('data_class', ChangePassword::class)
            ->setDefault('validation_groups', function () { return $this->validationGroups; })
            ->setRequired('require_current')
            ->setAllowedTypes('require_current', 'bool')
            ->setRequired('require_repeated')
            ->setAllowedTypes('require_repeated', 'bool');
    }
}
