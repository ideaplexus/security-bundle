<?php

namespace IPC\SecurityBundle\Entity;

class CoreUser implements UserInterface
{
    use CoreUserTrait;
}
