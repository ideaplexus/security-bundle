<?php

namespace IPC\SecurityBundle\Entity;

trait AdvancedUserTrait
{
    use CoreUserTrait;

    /**
     * User expired
     *
     * @var bool
     */
    protected $expired = false;

    /**
     * User locked
     *
     * @var bool
     */
    protected $locked = false;

    /**
     * User enabled
     *
     * @var bool
     */
    protected $enabled = false;

    /**
     * Credentials expired
     *
     * @var bool
     */
    protected $credentialsExpired = false;

    /**
     * @return bool
     *
     * @see AccountExpiredException
     */
    public function isExpired(): bool
    {
        return $this->expired;
    }

    /**
     * @param bool $expired
     *
     * @return $this
     */
    public function setExpired(bool $expired): self
    {
        $this->expired = $expired;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     *
     * @return $this
     *
     * @see LockedException
     */
    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * @return bool
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsExpired(): bool
    {
        return $this->credentialsExpired;
    }

    /**
     * @param bool $credentialsExpired
     *
     * @return $this
     */
    public function setCredentialsExpired(bool $credentialsExpired): self
    {
        $this->credentialsExpired = $credentialsExpired;

        return $this;
    }

    /**
     * @return bool
     *
     * @see DisabledException
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return $this
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            $this->username,
            $this->password,
            $this->salt,
            $this->locked,
            $this->expired,
            $this->credentialsExpired,
            $this->enabled,
            $this->roles,
        ]);
    }

    /**
     * @param string $serialized
     *
     * @return void
     */
    public function unserialize($serialized): void
    {
        [
            $this->username,
            $this->password,
            $this->salt,
            $this->locked,
            $this->expired,
            $this->credentialsExpired,
            $this->enabled,
            $this->roles,
        ] = unserialize($serialized, ['allowed_classes' => [self::class]]);
    }
}
