<?php

namespace IPC\SecurityBundle\Entity;

use Serializable;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface as CoreUserInterface;

interface UserInterface extends CoreUserInterface, EquatableInterface, Serializable
{
}
