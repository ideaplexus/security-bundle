<?php

namespace IPC\SecurityBundle\User;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class DoctrineUserProvider implements UserProviderInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * @var array
     */
    protected $userClasses = [];

    /**
     * @param ManagerRegistry $managerRegistry
     * @param array           $userClasses
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(ManagerRegistry $managerRegistry, array $userClasses)
    {
        $this->managerRegistry = $managerRegistry;

        foreach ($userClasses as $userClass => $identifiers) {
            $objectManager = $this->managerRegistry->getManagerForClass($userClass);

            if (!$objectManager) {
                throw new \InvalidArgumentException('Configured user class is not managed by doctrine.');
            }

            $classMetadata = $objectManager->getClassMetadata($userClass);

            $identifiers = (array) $identifiers;
            $identifiers = empty($identifiers) ? $classMetadata->getIdentifierFieldNames() : $identifiers;

            foreach ($identifiers as $field) {
                if (!$classMetadata->hasField($field)) {
                    throw new \InvalidArgumentException(sprintf(
                        'Configured identifier "%s" for user class "%s" does not exist.',
                        $field,
                        $userClass
                    ));
                }
            }

            $this->userClasses[$userClass] = $identifiers;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username): UserInterface
    {
        foreach ($this->userClasses as $userClass => $identifiers) {
            /* @var EntityRepository $repository */
            $repository = $this
                ->managerRegistry
                ->getManagerForClass($userClass)
                ->getRepository($userClass);

            try {
                $queryBuilder = $repository->createQueryBuilder('u');

                foreach ($identifiers as $property) {
                    $queryBuilder->orWhere('u.' . $property . ' = :' . $property);
                    $queryBuilder->setParameter($property, $username);
                }

                return $queryBuilder->getQuery()->getSingleResult();
            } catch (NoResultException $e) {
                $exception = new UsernameNotFoundException('Username could not be found.', $e->getCode(), $e);
                $exception->setUsername($username);
                continue;
            } catch (NonUniqueResultException $e) {
                $exception = new UsernameNotFoundException('Multiple users found by username field.', $e->getCode(), $e);
                $exception->setUsername($username);
                break;
            }
        }

        throw $exception;
    }

    /**
     * {@inheritdoc}
     *
     * @throws UsernameNotFoundException
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        $class = \get_class($user);

        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class): bool
    {
        return array_key_exists($class, $this->userClasses);
    }
}
