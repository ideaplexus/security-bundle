<?php

namespace Tests\IPC\SecurityBundle\Form\Type;

use IPC\SecurityBundle\Form\Type\LoginType;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @coversDefaultClass \IPC\SecurityBundle\Form\Type\LoginType
 */
class LoginTypeTest extends TypeTestCase
{
    /**
     * @return void
     *
     * @covers ::configureOptions()
     */
    public function testConfigureOptions(): void
    {
        $resolver = new OptionsResolver();
        $form     = new LoginType();

        $form->configureOptions($resolver);

        $this->assertTrue($resolver->hasDefault('remember_me'));

        $options = $resolver->resolve(['remember_me' => false]);

        $this->assertEquals(false, $options['remember_me']);
    }

    /**
     * @return void
     *
     * @covers ::buildForm()
     */
    public function testBuildForm(): void
    {
        $resolver = new OptionsResolver();
        $form     = new LoginType();

        $form->configureOptions($resolver);
        $form->buildForm($this->builder, $resolver->resolve());

        $this->assertEquals(Request::METHOD_POST, $this->builder->getMethod());

        $this->assertCount(4, $this->builder);

        $this->assertTrue($this->builder->has('username'));
        $username = $this->builder->get('username');
        $this->assertFalse($username->getOption('required'));
        $this->assertEquals('form.type.login.username.label', $username->getOption('label'));

        $this->assertTrue($this->builder->has('password'));
        $password = $this->builder->get('password');
        $this->assertFalse($password->getOption('required'));
        $this->assertEquals('form.type.login.password.label', $password->getOption('label'));

        $this->assertTrue($this->builder->has('remember_me'));
        $rememberMe = $this->builder->get('remember_me');
        $this->assertFalse($rememberMe->getOption('required'));
        $this->assertEquals('form.type.login.remember_me.label', $rememberMe->getOption('label'));

        $this->assertTrue($this->builder->has('submit'));

        $this->builder->all(); // fix for FormBuilder vs SubmitFormBuilder issue

        $this->assertTrue($this->builder->has('submit'));
        $submit = $this->builder->get('submit');
        $this->assertEquals('form.type.login.submit.label', $submit->getOption('label'));
    }
}
