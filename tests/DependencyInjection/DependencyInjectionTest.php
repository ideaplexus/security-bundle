<?php

namespace IPC\Tests\SecurityBundle\DependencyInjection;

use IPC\SecurityBundle\DependencyInjection\IPCSecurityExtension;
use IPC\SecurityBundle\Form\Type\ChangePasswordType;
use IPC\SecurityBundle\Form\Type\LoginType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Yaml\Parser;

class DependencyInjectionTest extends TestCase
{
    /**
     * @var IPCSecurityExtension
     */
    protected $extension;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->extension = new IPCSecurityExtension();
    }

    /**
     * @param array|null $customConfig
     *
     * @return ContainerBuilder
     */
    protected function getCustomizedContainer(?array $customConfig): ContainerBuilder
    {
        $ymlFile = __DIR__ . '/../_files/config.yaml';
        $parser  = new Parser();
        $config  = $parser->parse(file_get_contents($ymlFile))['ipc_security'];
        if (null !== $config && null !== $customConfig) {
            $config = array_merge($config, $customConfig);
        }
        $this->extension->load([$config], $container = new ContainerBuilder());
        return $container;
    }

    /**
     * @return void
     *
     * @covers \IPC\SecurityBundle\DependencyInjection\IPCSecurityExtension::load()
     * @covers \IPC\SecurityBundle\DependencyInjection\Configuration::getConfigTreeBuilder()
     */
    public function testLoadDefault(): void
    {
        $config = $this->getCustomizedContainer(null)->getParameterBag()->all();
        $default = [
            'ipc_security.login.form' => LoginType::class,
            'ipc_security.login.view' => '@IPCSecurity/login.html.twig',
            'ipc_security.login.route' => 'login',
            'ipc_security.login.flash_bag' => [
                'enabled'   => false,
                'translate' => true,
                'type'      => [
                    'error' => 'error',
                ],
            ],
            'ipc_security.credentials_expired.enabled' => false,
            'ipc_security.credentials_expired.form' => ChangePasswordType::class,
            'ipc_security.credentials_expired.form_options' => [
                'require_current'  => false,
                'require_repeated' => true,
            ],
            'ipc_security.credentials_expired.view' => '@IPCSecurity/change_password.html.twig',
            'ipc_security.credentials_expired.route' => 'credentials_expired',
            'ipc_security.credentials_expired.target_route' => 'login',
            'ipc_security.credentials_expired.flash_bag' => [
                'enabled'   => false,
                'translate' => true,
                'type'      => [
                    'error'   => 'error',
                    'success' => 'success',
                ],
            ],
        ];

        $this->assertEquals($default, $config);
    }
}
