<?php

namespace IPC\Tests\SecurityBundle\User;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use IPC\SecurityBundle\Entity\CoreUser;
use IPC\SecurityBundle\User\DoctrineUserProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @coversDefaultClass \IPC\SecurityBundle\User\DoctrineUserProvider
 */
class DoctrineUserProviderTest extends TestCase
{
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->managerRegistry = $this->createMock(ManagerRegistry::class);
    }

    /**
     * @return void
     *
     * @throws \InvalidArgumentException
     *
     * @covers ::__construct()
     */
    public function test__constructUserClassNotManagedByDoctrine(): void
    {
        $className   = 'some_class';
        $identifier  = 'id';
        $userClasses = [$className => [$identifier]];

        $this->managerRegistry
            ->expects($this->once())
            ->method('getManagerForClass')
            ->with($className)
            ->willReturn(null);

        $this->expectException(\InvalidArgumentException::class);

        $this->getDoctrineUserProvider($userClasses);
    }

    /**
     * @return void
     *
     * @throws \InvalidArgumentException
     *
     * @covers ::__construct()
     */
    public function test__constructIdentifierForUserClassDoesNotExist(): void
    {
        $className   = 'some_class';
        $identifier  = 'id';
        $userClasses = [$className => [$identifier]];

        $objectManager = $this->createMock(ObjectManager::class);
        $classMetadata = $this->createMock(ClassMetadata::class);

        $this->managerRegistry
            ->expects($this->exactly(1))
            ->method('getManagerForClass')
            ->with($className)
            ->willReturn($objectManager);

        $objectManager
            ->expects($this->once())
            ->method('getClassMetadata')
            ->with($className)
            ->willReturn($classMetadata);

        $classMetadata
            ->expects($this->once())
            ->method('hasField')
            ->with($identifier)
            ->willReturn(false);

        $this->expectException(\InvalidArgumentException::class);

        $this->getDoctrineUserProvider($userClasses);
    }

    /**
     * @return void
     *
     * @throws \ReflectionException
     *
     * @covers ::__construct()
     */
    public function test__construct(): void
    {
        $className   = 'some_class';
        $identifier  = 'id';
        $userClasses = [$className => []]; // empty identifier to get them from class metadata

        $objectManager = $this->createMock(ObjectManager::class);
        $classMetadata = $this->createMock(ClassMetadata::class);

        $this->managerRegistry
            ->expects($this->exactly(1))
            ->method('getManagerForClass')
            ->with($className)
            ->willReturn($objectManager);

        $objectManager
            ->expects($this->once())
            ->method('getClassMetadata')
            ->with($className)
            ->willReturn($classMetadata);

        $classMetadata
            ->expects($this->once())
            ->method('getIdentifierFieldNames')
            ->willReturn([$identifier]);

        $classMetadata
            ->expects($this->once())
            ->method('hasField')
            ->with($identifier)
            ->willReturn(true);

        $doctrineUserProvider = $this->getDoctrineUserProvider($userClasses);

        $reflectionProperty = new \ReflectionProperty($doctrineUserProvider, 'userClasses');
        $reflectionProperty->setAccessible(true);

        $userClasses = $reflectionProperty->getValue($doctrineUserProvider);

        $this->assertCount(1, $userClasses);
        $this->assertArrayHasKey($className, $userClasses);
        $this->assertEquals([$identifier], $userClasses[$className]);
    }

    /**
     * @return array
     */
    public function providerLoadUserByUsername(): array
    {
        return [
            [
                $this->createMock(UserInterface::class),
            ],
            [
                $this->createMock(NoResultException::class),
            ],
            [
                $this->createMock(NonUniqueResultException::class),
            ],
        ];
    }

    /**
     * @param mixed $output
     *
     * @return void
     *
     * @dataProvider providerLoadUserByUsername()
     *
     * @covers ::loadUserByUsername()
     */
    public function testLoadUserByUsername($output): void
    {
        $className   = 'some_class';
        $identifier  = 'id';
        $userClasses = [];
        $username    = 'test';

        $objectManager        = $this->createMock(ObjectManager::class);
        $objectRepository     = $this->createMock(EntityRepository::class);
        $queryBuilder         = $this->createMock(QueryBuilder::class);
        $query                = $this->createMock(AbstractQuery::class);

        $doctrineUserProvider = $this->getDoctrineUserProvider($userClasses);

        $reflection = new \ReflectionProperty($doctrineUserProvider, 'managerRegistry');
        $reflection->setAccessible(true);
        $reflection->setValue($doctrineUserProvider, $this->managerRegistry);

        $reflection = new \ReflectionProperty($doctrineUserProvider, 'userClasses');
        $reflection->setAccessible(true);
        $reflection->setValue($doctrineUserProvider, [$className => [$identifier]]);

        $this->managerRegistry
            ->expects($this->once())
            ->method('getManagerForClass')
            ->willReturn($objectManager);

        $objectManager
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($objectRepository);

        $objectRepository
            ->expects($this->once())
            ->method('createQueryBuilder')
            ->with('u')
            ->willReturn($queryBuilder);

        $queryBuilder
            ->expects($this->once())
            ->method('orWhere')
            ->with("u.$identifier = :$identifier");

        $queryBuilder
            ->expects($this->once())
            ->method('setParameter')
            ->with($identifier, $username);

        $queryBuilder
            ->expects($this->once())
            ->method('getQuery')
            ->willReturn($query);


        if ($output instanceof \Exception) {
            $query
                ->expects($this->once())
                ->method('getSingleResult')
                ->willThrowException($output);

            $this->expectException(UsernameNotFoundException::class);

           $doctrineUserProvider->loadUserByUsername($username);
        } else {
            $query
                ->expects($this->once())
                ->method('getSingleResult')
                ->willReturn($output);

            $this->assertEquals($output, $doctrineUserProvider->loadUserByUsername($username));
        }
    }

    /**
     * @return void
     *
     * @covers ::refreshUser()
     */
    public function testRefreshUserUnsupportedUser(): void
    {
        $user = $this->createMock(CoreUser::class);

        $doctrineUserProvider = $this->getMockBuilder(DoctrineUserProvider::class)
            ->disableOriginalConstructor()
            ->setMethods(['supportsClass'])
            ->getMock();

        $doctrineUserProvider
            ->expects($this->once())
            ->method('supportsClass')
            ->with(\get_class($user))
            ->willReturn(false);

        $this->expectException(UnsupportedUserException::class);

        $doctrineUserProvider->refreshUser($user);
    }

    /**
     * @return void
     *
     * @covers ::refreshUser()
     */
    public function testRefreshUser(): void
    {
        $username = 'test';
        $user     = $this->createMock(CoreUser::class);

        $user
            ->expects($this->once())
            ->method('getUsername')
            ->willReturn($username);

        $doctrineUserProvider = $this->getMockBuilder(DoctrineUserProvider::class)
            ->disableOriginalConstructor()
            ->setMethods(['supportsClass', 'loadUserByUsername'])
            ->getMock();

        $doctrineUserProvider
            ->expects($this->once())
            ->method('supportsClass')
            ->with(\get_class($user))
            ->willReturn(true);

        $doctrineUserProvider
            ->expects($this->once())
            ->method('loadUserByUsername')
            ->with($username)
            ->willReturn($user);

        $doctrineUserProvider->refreshUser($user);
    }

    /**
     * @return void
     *
     * @covers ::supportsClass()
     */
    public function testSupportsClass(): void
    {
        $className   = 'some_class';
        $identifier  = 'id';
        $userClasses = [];

        $doctrineUserProvider = $this->getDoctrineUserProvider($userClasses);

        $reflection = new \ReflectionProperty($doctrineUserProvider, 'userClasses');
        $reflection->setAccessible(true);
        $reflection->setValue($doctrineUserProvider, [$className => [$identifier]]);

        $this->assertTrue($doctrineUserProvider->supportsClass($className));
        $this->assertFalse($doctrineUserProvider->supportsClass('some_other_class'));
    }

    /**
     * @param array $userClasses
     *
     * @return DoctrineUserProvider
     */
    protected function getDoctrineUserProvider(array $userClasses): DoctrineUserProvider
    {
        return new DoctrineUserProvider($this->managerRegistry, $userClasses);
    }
}
